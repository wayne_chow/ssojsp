<%@ page import="java.util.Enumeration" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%
    response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
    response.addHeader("Pragma", "no-cache");
    response.addDateHeader ("Expires", 0);
    
    Enumeration<String> attributeNames = session.getAttributeNames();
    while(attributeNames.hasMoreElements()){
        session.removeAttribute(attributeNames.nextElement());
    }
    
    //Remove the session
    session.invalidate();

    String timeout = request.getParameter("timeout");
    if(timeout == null || timeout.isEmpty()){
        response.sendRedirect("index.jsp");
    }else{
        response.sendRedirect("index.jsp?timeout="+timeout );
    }


%>