<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%
    response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
    response.addHeader("Pragma", "no-cache");
    response.addDateHeader ("Expires", 0);
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/cookie/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/properties.js"></script>
        <script type="text/javascript" src="../js/jquery.iecors.js"></script>
        <title>ｉ服務平台</title>
        <style type="text/css">

            #mainContent
                {
                    width:1280px;
                    height:800px;
                    background-image: url("../img/P1.jpeg");
                    background-repeat: no-repeat;
                    background-position: center ;
                }

        </style>
        <script>

            $(document).ready(function(){
                var cookieOption = { path: '/', expires: 1 };

                $("#buttonPanel").hide();
<%
                String timeout = request.getParameter("timeout");
                if(timeout == null || !timeout.equals("Y")){
%>
                    $("#message").hide();
<%
                }else{
%>
                    $("#message").text("會話超時，請重入登入。");
                    $.cookie("COOKIE_USERID", null, cookieOption);
                    $.cookie("COOKIE_PASSWORD", null, cookieOption);
                    //$.cookie("COOKIE_LANGUAGE", null, cookieOption);
<%
                }
%>
                
                var cookieUserId   = $.cookie("COOKIE_USERID");
                var cookiePassword = $.cookie("COOKIE_PASSWORD");
                //var cookieLanguage = $.cookie("COOKIE_LANGUAGE");

                if(cookieUserId != null)
                    $("#SSOForm input[name=userid]").val(cookieUserId);
                if(cookiePassword != null)
                    $("#SSOForm input[name=password]").val(cookiePassword);
                //if(cookieLanguage != null)
                //    $("#SSOForm select[name=language]").val(cookieLanguage);

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);


                //$("#SSOForm select[name=language]").change(function(){
                $("#SSOForm label[name=language]").click(function(){
                    //var language = $("option:selected", this).val();
                    var language = $(this).attr("value");
                    var userId   = $("#SSOForm input[name=userid]").val();
                    var password = $("#SSOForm input[name=password]").val();

                    var date = new Date();
                    date.setTime(date.getTime() + (60 * 1000));
                    $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                    $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });

                    var languageDate = new Date();
                    languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                    $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                    changeLanguage(language);
                });

                $("#SSOForm input").keypress(function(e) {
                        if(e.which == 13 && $("#btn_checkLogin").is(":visible")) {
                            checkLogin();
                        }
                    });

                $("#btn_checkLogin").click(checkLogin);

                $("#SSOForm input[name=userid]").focus();
            });

            function checkLogin(){
                var userid = $("#SSOForm input[name=userid]").val();
                var password = $("#SSOForm input[name=password]").val();
                if(userid == null || userid == ""){//validate the input user id
                    $("#message").text("用戶號碼不能空白");
                    $("#message").show();
                }else if(password == null || password == ""){//validate the input password
                    $("#message").text("密碼不能空白");
                    $("#message").show();
                }else{
                    $("#message").hide();
                    $("#message").text("");

                    var psmUserValid = false;
                    var wsmUserValid = false;

                    $.support.cors = true;
                    //check psm login
                    $.ajax({
                        async: false,
                        type: "GET",
                        url: psmUserValidationLink,
                        data: {
                            "username": userid,
                            "password": password
                        },
                        success: function(data){
                            if(data == true){
                                psmUserValid = true;
                            }
                        },
                        complete: function(){
                            // check wsm login
                            $.ajax({
                                async: false,
                                type: "POST",
                                url: wsmUserValidationLink,
                                data: {
                                    "userid"    : userid,
                                    "password"  : password,
                                    "company"   : "AMG"
                                },
                                success: function(data){
                                    if(data == true){
                                        wsmUserValid = true;
                                    }
                                },
                                error: function(XMLHttpRequest,status,error){
                                    alert(error);
                                },
                                complete: function(){
                                    if(psmUserValid && wsmUserValid){//both system user valid, redirect to button menu page
                                        redirectToButtonMenu("Y", "Y");
                                    }else if(psmUserValid){//psm user valid, redirect to button menu page
                                        redirectToButtonMenu("Y", "N");
                                    }else if(wsmUserValid){//wsm user valid, redirect to button menu page
                                        redirectToButtonMenu("N", "Y");
                                    }else{//both system user invalid, show login fail msg for both system.
                                        var msg = "WSM登入失敗<br/>PSM登入失敗";
                                        $("#message").html(msg);
                                        $("#message").show();
                                    }
                                },
                                dataType: "json"
                            })
                        },
                        dataType: "json"
                    });


                }
            }


            //Change language function
            function changeLanguage(language){
                if(language == "zh_CN_"){
                    window.location.href = "../cn/index.jsp";
                }else if(language == "zh_TW_"){
                    window.location.href = "../zh/index.jsp";
                }else{
                    window.location.href = "../en/index.jsp";
                }
            }

            // go to buttonMenu.jsp
            function redirectToButtonMenu(isPSMValid, isWSMValid){
                var form = $("#buttonMenuForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='buttonMenuForm' target='_self' action='welcome.jsp'></form>");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='isPSMValid' value='' />");
                    form.append("<input type='hidden' name='isWSMValid' value='' />");
                    $("body").append(form);
                }
                form.children("input[name=userid]").val($("#SSOForm input[name=userid]").val());
                form.children("input[name=password]").val($("#SSOForm input[name=password]").val());
                form.children("input[name=language]").val("zh_TW_");
                form.children("input[name=isPSMValid]").val(isPSMValid);
                form.children("input[name=isWSMValid]").val(isWSMValid);
                form.submit();
            }
        </script>
    </head>
    <body lang="zh-cn">
    <center>
        <div id="mainContent">
        <form id="SSOForm">
            <table style="margin-right: 8px;">
                <tbody>
                    <tr>
                        <td rowspan="2" style="width: 960px;">
                            &nbsp;
                        </td>
                        <td style="vertical-align: top; width: 320px; height: 280px; text-align: center;">
                            <div style="padding-top:10px; color: #acce22; font-size:24px">
                                <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22">简体</label> |
                                <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22">繁體</label> |
                                <label name="language" value="en__" style="cursor: pointer; color: #acce22">English</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 12px; vertical-align: top; height: 587px;">
                            <table border="0" cellpadding="2" cellspacing="2">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle; width: 100px; text-align: center; font-family: Arial;">
                                            <big><span style="color: rgb(0, 0, 153);">用戶號碼</span> </big>
                                        </td>
                                        <td style="vertical-align: middle; width: 202px; text-align: center;">
                                            <input type="text" name="userid" tabindex="1" autofocus="" placeholder="用戶號碼" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; font-family: Arial; color: rgb(0, 0, 153); text-align: center;">
                                            <big>密碼</big>
                                        </td>
                                        <td style="vertical-align: middle; text-align: center;">
                                            <input type="password" name="password" tabindex="2" placeholder="密碼">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table style="width:100%;"border="0" cellpadding="2" cellspacing="2">
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="padding-top: 12px; vertical-align: middle; text-align: center;">
                                            <button type="button" id="btn_checkLogin" style="height:30px; padding-top:0px; padding-bottom:0px" class="btn btn-lg btn-primary">登入</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: center; padding: 5px 5px 0 0;">
                                            <a id="forgetPasswordLink"href="forgetPassword.jsp">忘記密碼</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div id="message" style="color:#ff0000; padding-left: 8px; text-align:center"></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
        </div>
    </center>
    </body>
</html>
