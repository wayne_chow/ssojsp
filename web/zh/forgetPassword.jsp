<%@ page import="java.util.Calendar" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%
    response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
    response.addHeader("Pragma", "no-cache");
    response.addDateHeader ("Expires", 0);
%>
<html>
<head>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/general/general.css"/>
    <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="../js/properties.js"></script>
    <script type="text/javascript" src="../js/jquery.iecors.js"></script>

    <style type="text/css">
            #mainContent
                {
                    width:1280px;
                    height:800px;
                    background-image: url("../img/P1.jpeg");
                    background-repeat: no-repeat;
                    background-position: center ;
                }

    </style>

    <script type="text/javascript">
        var requestAuthTime = (new Date()).getTime();
        
        $(document).ready(function(){
            //reset the form
            $(":input[type=text]").val("");
            $("#refreshAuthCode").mouseover(function(e) {$(this).addClass('ui-state-hover');})
                                .mouseout(function(e) {$(this).removeClass('ui-state-hover');});

            
            $("#backButton, #doneButton, #backButtonHome").click(function(){
                window.location.href = "index.jsp";
                
            });


            $("#langPanel label[name=language]").click(function(){
                var language = $(this).attr("value");
                var languageDate = new Date();
                languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                changeLanguage(language);
            });

            //Change language function
            function changeLanguage(language){
                if(language == "zh_CN_"){
                    window.location.href = "../cn/forgetPassword.jsp";
                }else if(language == "zh_TW_"){
                    window.location.href = "../zh/forgetPassword.jsp";
                }else{
                    window.location.href = "../en/forgetPassword.jsp";
                }
            }


            $("#refreshAuthCode").click(function(){
                requestAuthTime = (new Date()).getTime();
                $("#authCodeImage").attr("src", authCodePath + "?" + requestAuthTime);
            });
            $("#refreshAuthCode").click();

            $('#myModal').on('shown.bs.modal', function() {
                var initModalHeight = $('.modal-dialog').outerHeight(); //give an id to .mobile-dialog
                var userScreenHeight = $(document).outerHeight();
                if (initModalHeight > userScreenHeight) {
                    $('.modal-dialog').css('overflow', 'auto'); //set to overflow if no fit
                } else {
                    $('.modal-dialog').css('margin-top',
                    (userScreenHeight / 2) - (initModalHeight/2)); //center it if it does fit
                }
            });

            $("#submitButton").click(function(){

                $("#errorMessage").text("");

                var userid = $("#userid").val();
                var authCode = $("#authCode").val();

                if(userid == null || userid == "" || authCode == null || authCode == ""){
                    $("#errorMessage").text("請輸入用戶號碼及驗證碼。");
                    return false;
                }

                $("#errorMessage").text("");
                $("#submitButton").css("");
                var dataArray = $("#forgetPasswordForm").serializeArray();
                dataArray.push({name: 'requestAuthTime', value: requestAuthTime});
                $.support.cors = true;
                $.ajax({
                    type:'POST',
                    url:submitPath,
                    cahch:false,
                    data: dataArray,
                    dataType:"json",
                    xhrFields: {//cross domains
                        withCredentials: true
                    },
                    success: function(json){
                        if(json.authCode){

                            $("#step1").find("tr").not(":nth-child(1)").not(":nth-child(2)").not(":nth-child(7)").remove();
                            $("#submitButton").remove();
                            $("#step1").find("tr:nth-child(2)").html(json.message);

                            if(json.isSuccess){
                                $("#doneButton").show();
                                $("#retryButton").hide();
                                $("#backButtonHome").hide();
                            }else{
                                $("#doneButton").hide();
                                $("#retryButton").show();
                                $("#backButtonHome").show();
                            }

                            //success/error message
                            $("#successMessage").html("<p>" + json.message + "</p>");
                            $("#step2").hide(1500, function(){
                                $("#step3").show("slow");
                            });

                        }else{
                            /*
                            if(jQuery.browser.msie){
                                $('#myModal').modal('show');
                                //$("#errorMessage").text("瀏覽器不支援此功能，請聯絡i.service3388@gmail.com以更改密碼");
                            }else{*/
                                //error message
                                $("#authCode").val("");
                                $("#errorMessage").text(json.message);
                                $("#refreshAuthCode").trigger("click");
                            //}
                        }
                    },
                    error: function(XMLHttpRequest,status,error){
                        alert(error);
                    }
                });
                /*
                $.post(submitPath, $("#forgetPasswordForm").serializeArray(),
                    function(json) {
                        if(json.authCode){

                            $("#step1").find("tr").not(":nth-child(1)").not(":nth-child(2)").not(":nth-child(7)").remove();
                            $("#submitButton").remove();
                            $("#step1").find("tr:nth-child(2)").html(json.message);
                            
                            if(json.isSuccess){
                                $("#doneButton").show();
                                $("#retryButton").hide();
                                $("#backButtonHome").hide();
                            }else{
                                $("#doneButton").hide();
                                $("#retryButton").show();
                                $("#backButtonHome").show();
                            }
                            
                            
                            //success/error message
                            $("#successMessage").html("<p>" + json.message + "</p>");
                            $("#step2").hide(1500, function(){
                                $("#step3").show("slow");
                            });

                        }else{
                            //error message
                            $("#authCode").val("");
                            $("#errorMessage").text(json.message);
                            $("#refreshAuthCode").trigger("click");
                        }
                        
                    },"json"
                );
                */
            });
        });
    </script>
    <title>ｉ服務平台</title>
</head>

<body style=" ">
    <center>
        <div id="mainContent">
    <table style="margin-right:8px;">
        <tr>
            <td rowspan="3" style="width: 960px;">
                &nbsp;
            </td>
            <td style="vertical-align: top; width: 320px; height: 260px; text-align: center;">
                <div id="langPanel" style="padding-top:10px; color: #acce22; font-size:24px">
                    <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22">简体</label> |
                    <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22">繁體</label> |
                    <label name="language" value="en__" style="cursor: pointer; color: #acce22">English</label>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" style="color: rgb(0, 0, 153);">
                    <form id="forgetPasswordForm" method="post" action="#">
                    <table id="step1" width="300px;">
                        <tr align="center">
                            <td colspan="5" style="font-size:30px">忘記密碼<hr/></td>
                         </tr>
                         <tr align="left">
                             <td colspan="5">重置密碼，請輸入您的用戶號碼及圖片中顯示的字符。</td>
                         </tr>
                         <tr align="left">
                             <td colspan="5">&nbsp;</td>
                         </tr>
                         <tr align="left">
                            <td>&nbsp;</td>
                            <td style="width:60px">用戶號碼</td>
                            <td style="width:10px">:</td>
                            <td style="width:100px">
                                <input type="text" class="text" tabindex="1" id="userid" name="userid" maxlength="12" style="width:150px"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr align="left">
                            <td style="width:30px">&nbsp;</td>
                            <td style="width:100px">驗證碼</td>
                            <td>:</td>
                            <td><input type="text" class="text" tabindex="3" id="authCode" name="authCode" style="width:150px"/></td>
                            <td style="width:30px">&nbsp;</td>
                        </tr>
                        <tr align="right">
                            <td style="width:30px">&nbsp;</td>
                            <td colspan="3"><img id="authCodeImage" style="width:100%; height:60px;"/></td>
                            <script>$("#authCodeImage").attr("src", authCodePath + "?" + requestAuthTime);</script>
                            <td style="vertical-align:bottom"><div id="refreshAuthCode" class="ui-state-default ui-corner-all" style="width:22px;"><span class="ui-icon ui-icon-refresh"/></div></td>
                        </tr>
                        <tr align="left">
                             <td colspan="5" align="center">
                                <br>
                                <input id="submitButton" type="button" tabindex="4" style="height:30px; padding-top:0px; padding-bottom:0px;" class="btn btn-lg btn-primary" value="提交"/>
                                <input id="backButton" type="button" tabindex="5" style="height:30px; padding-top:0px; padding-bottom:0px;" class="btn btn-lg btn-primary" value="返回"/>
                             </td>
                        </tr>
                   </table>
              </form>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;">
                <p id="errorMessage" style="color:#ff0000; padding-left: 8px;"></p>
            </td>
        </tr>
    </table>
            </div>
    </center>
<%--
    <div id="dialog-message" title="訊息">
        <p>
        瀏覽器不支援此功能，請聯絡 i.service3388@gmail.com 以更改密碼
        </p>
    </div>--%>

    <div class="modal fade" id="myModal"tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog"  style="width:50%">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">訊息</h4>
          </div>
          <div class="modal-body">
            瀏覽器不支援此功能，請聯絡 i.service3388@gmail.com 以更改密碼
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>
