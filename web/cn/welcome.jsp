<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%
        response.setContentType("text/html;charset=UTF-8");

        response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0");
        response.addHeader("Pragma", "no-cache");
        response.addDateHeader ("Expires", 0);
/*
        String userId = (String)session.getAttribute("userid");
        String password = (String)session.getAttribute("password");
        String language = (String)session.getAttribute("language");
        String isWSMValid = (String)session.getAttribute("isWSMValid");
        String isPSMValid = (String)session.getAttribute("isPSMValid");
*/
        String userId = request.getParameter("userid");
        String password = request.getParameter("password");
        String language = request.getParameter("language");
        String isWSMValid = request.getParameter("isWSMValid");
        String isPSMValid = request.getParameter("isPSMValid");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../css/bootstrap.css" rel="stylesheet">
        <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/cookie/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/properties.js"></script>
        <title>ｉ服务平台</title>
        <style type="text/css">
            body {
                margin: 0px auto;
                padding: 0px;                
                background-position:  center center;
                width: 1280px;
                vertical-align: top;
                background-color: #cccccc;
                background-repeat: no-repeat;
                height: 100%;
                text-align: left;
            }

            div.button{
                height: 172px;
                width: 172px;
            }
            
            div.clickable{
                cursor : pointer;
            }

            div.blocked{
                cursor : not-allowed;
            }
        </style>

        <script>

            $(document).ready(function(){

                var timeOut = 1000 * 60 * 15; // 15 minutes
                var lastActivity = new Date().getTime();
                var checkTimeOut = function(){
                    if(new Date().getTime() > lastActivity + timeOut){
                        // redirect to timeout page
                        window.location.href = "logout.jsp?timeout=Y";
                    }else{
                        window.setTimeout(checkTimeOut, 1000); // check once per second
                    }
                }
                checkTimeOut();
<%
                if(isWSMValid != null && isWSMValid.equals("Y") &&
                        isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $("div.button").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#wsm").click(loginWSM);
                    $("#psm").click(loginPSM);
                    $("#prod").click(function(){window.open("http://info.i-services.asia/market-m.asp", "prod")});
                    $("#vip").click(function(){window.open("http://info.i-services.asia/special-account.asp", "vip")});
                    $("#market").click(function(){window.open("http://info.i-services.asia/confinement.asp", "market")});
<%
                }else if(isPSMValid != null && isPSMValid.equals("Y")){
%>
                    $("div.button:not(#wsm)").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#psm").click(loginPSM);
                    $("#prod").click(function(){window.open("http://info.i-services.asia/market-m.asp", "prod")});
                    $("#vip").click(function(){window.open("http://info.i-services.asia/special-account.asp", "vip")});
                    $("#market").click(function(){window.open("http://info.i-services.asia/confinement.asp", "market")});
<%
                }else if(isWSMValid != null && isWSMValid.equals("Y")){
%>
                    $("div.button:not(#psm)").removeClass("blocked").addClass("clickable").mouseover(function(){
                        $(this).css("background-image", $(this).attr("overImage"));
                    }).mouseout(function(){
                        $(this).css("background-image", $(this).attr("outImage"));
                    });
                    $("#wsm").click(loginWSM);
                    $("#prod").click(function(){window.open("http://info.i-services.asia/market-m.asp", "prod")});
                    $("#vip").click(function(){window.open("http://info.i-services.asia/special-account.asp", "vip")});
                    $("#market").click(function(){window.open("http://info.i-services.asia/confinement.asp", "market")});
<%
                }
%>

                var cookieOption = { path: '/', expires: 1 };

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);

                var cookieUserId   = $.cookie("COOKIE_USERID");
                var cookiePassword = $.cookie("COOKIE_PASSWORD");

                if(cookieUserId != null)
                    $(".param input[name=userid]").val(cookieUserId);
                if(cookiePassword != null)
                    $(".param input[name=password]").val(cookiePassword);

                $.cookie("COOKIE_USERID", null, cookieOption);
                $.cookie("COOKIE_PASSWORD", null, cookieOption);

                $("label[name=language]").click(function(){
                    //var language = $("option:selected", this).val();
                    var language = $(this).attr("value");
                    var userId   = $(".param input[name=userid]").val();
                    var password = $(".param input[name=password]").val();

                    var date = new Date();
                    date.setTime(date.getTime() + (60 * 1000));
                    $.cookie("COOKIE_USERID", userId, { path: '/', expires: date });
                    $.cookie("COOKIE_PASSWORD", password, { path: '/', expires: date });

                    var languageDate = new Date();
                    languageDate.setTime(languageDate.getTime() + (30 * 24 * 60 * 60 * 1000));//30 days
                    $.cookie("COOKIE_LANGUAGE", language, { path: '/', expires: languageDate });

                    changeLanguage(language);
                });

                $("#btn_release").click(release);
            });


            function release(){
                window.location.href = "logout.jsp";
            }

            // login to wsm and redirect the user to the new page
            function loginWSM(){
                var form = $("#WSMLoginForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='WSMLoginForm' target='wsm' action='" + wsmLoginLink + "'></form>");
                    form.append("<input type='hidden' name='origin' value='SSO' />");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='company' value='AMG' />");
                    $("body").append(form);
                }
                form.children("input[name=userid]").val($(".param input[name=userid]").val());
                form.children("input[name=password]").val($(".param input[name=password]").val());
                form.children("input[name=language]").val($(".param input[name=language]").val());
                form.submit();
            }

            // login to psm and redirect the user to the new page
            function loginPSM(){
                var form = $("#PSMLoginForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='PSMLoginForm' target='psm' action='" + psmLoginLink + "'></form>");
                    form.append("<input type='hidden' name='Origin' value='sso' />");
                    form.append("<input type='hidden' name='UserId' value='' />");
                    form.append("<input type='hidden' name='Password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    $("body").append(form);
                }
                form.children("input[name=UserId]").val($(".param input[name=userid]").val());
                form.children("input[name=Password]").val($(".param input[name=password]").val());
                //convert to psm variable
                var lang = $(".param input[name=language]").val();
                if(lang == "en__"){
                    lang = "1";
                }else if(lang == "zh_TW_"){
                    lang = "2";
                }else if(lang == "zh_CN_"){
                    lang = "3";
                }else{
                    lang = "1";
                }
                form.children("input[name=language]").val(lang);
                form.submit();
            }

            //Change language function
            function changeLanguage(language){
                var form = $("#buttonMenuForm");
                if(form.length == 0){//create form
                    var form = $("<form method='post' id='buttonMenuForm' target='_self' action='welcome.jsp'></form>");
                    form.append("<input type='hidden' name='userid' value='' />");
                    form.append("<input type='hidden' name='password' value='' />");
                    form.append("<input type='hidden' name='language' value='' />");
                    form.append("<input type='hidden' name='isPSMValid' value='' />");
                    form.append("<input type='hidden' name='isWSMValid' value='' />");
                    $("body").append(form);
                }
                var actionHref = "../en/welcome.jsp";
                if(language == "zh_CN_"){
                    actionHref = "../cn/welcome.jsp";
                }else if(language == "zh_TW_"){
                    actionHref = "../zh/welcome.jsp";
                }
                form.attr("action", actionHref);
                form.children("input[name=userid]").val("<%=userId%>");
                form.children("input[name=password]").val("<%=password%>");
                form.children("input[name=language]").val(language);
                form.children("input[name=isPSMValid]").val("<%=isPSMValid%>");
                form.children("input[name=isWSMValid]").val("<%=isWSMValid%>");
                form.submit();
            }

        </script>
    </head>
    <body style="height: 781px;">
        <table style="text-align: left; width: 1280px; height: 775px;background-image: url('../img/P2.jpeg');" border="0" cellpadding="0" cellspacing="2">
            <tbody>
                <tr>
                    <td style="vertical-align: top; width: 300px; height: 46px;"> <br>
                        <div class="param">
                        <input type="hidden" name="userid" value="<%=(userId==null)?"":userId%>"/>
                        <input type="hidden" name="password" value="<%=(password==null)?"":password%>"/>
                        <input type="hidden" name="language" value="<%=(language==null)?"":language%>"/>
                        </div>
                    </td>
                    <td style="vertical-align: top; width: 172px;" > <br> </td>
                    <td style="vertical-align: top; width: 78px;"> <br> </td>
                    <td style="vertical-align: top; width: 172px;"> <br> </td>
                    <td style="vertical-align: top; width: 78px;"> <br> </td>
                    <td style="vertical-align: top; width: 172px;"> <br> </td>
                    <td style="vertical-align: top; height: 46px; text-align:center">
                        <div style="padding-top:10px; color: #acce22; font-size:24px">
                            <label name="language" value="zh_CN_" style="cursor: pointer; color: #acce22">简体</label> |
                            <label name="language" value="zh_TW_" style="cursor: pointer; color: #acce22">繁體</label> |
                            <label name="language" value="en__" style="cursor: pointer; color: #acce22">English</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"> <br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked" id="psm" style="background:url(../img/1-1.png)" outImage="url(../img/1-1.png)" overImage="url(../img/1.png)"/>
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked"  id="prod" style="background:url(../img/2-1.png)" outImage="url(../img/2-1.png)" overImage="url(../img/2.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked"  id="vip" style="background:url(../img/3-1.png)" outImage="url(../img/3-1.png)" overImage="url(../img/3.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td style="height: 44px;" colspan="7"><br></td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"><br> </td>
                    <td style="vertical-align: top;">
                        <div class="button blocked" id="wsm" style="background:url(../img/4-1.png)" outImage="url(../img/4-1.png)" overImage="url(../img/4.png)"/>
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="trust" style="background:url(../img/5-1.png)" outImage="url(../img/5-1.png)" overImage="url(../img/5.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="market" style="background:url(../img/6-1.png)" outImage="url(../img/6-1.png)" overImage="url(../img/6.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td style="height: 44px;" colspan="7"><br></td>
                </tr>
                <tr>
                    <td style="vertical-align: top; height: 172px;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="service" style="background:url(../img/7-1.png)" outImage="url(../img/7-1.png)" overImage="url(../img/7.png)"  />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="land" style="background:url(../img/8-1.png)" outImage="url(../img/8-1.png)" overImage="url(../img/8.png)"  />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                    <td style="vertical-align: top;" >
                        <div class="button blocked" id="other" style="background:url(../img/9-1.png)" outImage="url(../img/9-1.png)" overImage="url(../img/9.png)" />
                    </td>
                    <td style="vertical-align: top;"><br> </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align:center">
                        <button type="button" id="btn_release" class="btn btn-lg btn-primary">离开</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>